# Vector figure template

Check Makefile for usage.

`make build` will build a PDF with everything inside the Inkscape canvas.
To render only one object, add something like `--export-id="main"` to the build command.
